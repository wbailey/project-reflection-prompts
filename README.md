# Project Reflection Prompts for In-Person Grading

### 1. Initial Goals and Objectives

- **Describe the goals or objectives you set for yourself at the outset of this project.** What were you hoping to achieve or learn?
- **Reflect on how well you achieved your goals.** Which objectives did you meet, exceed, or fall short of? What factors contributed to these outcomes?

### 2. Demonstration of Functionality

- **Provide a demonstration of your project's functionality.** Walk us through the key features and how they operate. What makes your project work as intended?

### 3. Explanation of Code Structure

- **Discuss your code, starting at the main entry point.** How is your code organized? Walk us through the major components and their interactions. Why did you choose this structure?
- **What were the biggest challenges or roadblocks you faced during the development of this project?** Describe specific instances and why they were challenging.
- **How did you overcome the challenges or roadblocks you encountered?** Share your strategies, resources, or changes in approach that helped you move forward.

### 4. Key Takeaways and Learnings

- **Reflect on your biggest takeaways from this project.** What are the main things you learned? How has this project contributed to your growth as a developer?
- **Based on what you've learned, what are the next steps for you?** How do you plan to build on the knowledge and skills gained from this project?
